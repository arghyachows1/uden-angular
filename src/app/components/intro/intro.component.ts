import { Component, OnInit, HostListener, Inject } from '@angular/core';
import {
  trigger,
  state,
  transition,
  style,
  animate,
} from '@angular/animations';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'uden-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss'],
})
export class IntroComponent implements OnInit {
  constructor(@Inject(DOCUMENT) document) {}

  sectiontwo = false;
  sectionthree = false;
  ngOnInit(): void {}

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 150 && window.pageYOffset < 600) {
      this.sectiontwo = true;
    } else if (window.pageYOffset > 600) {
      this.sectionthree = true;
    }
  }
}
