import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from './../../../interfaces/user.interface';

@Component({
  selector: 'uden-individual-register',
  templateUrl: './individual-register.component.html',
  styleUrls: ['./individual-register.component.scss'],
})
export class IndividualRegisterComponent implements OnInit {
  constructor(private userService: UserService) {}
  user: User;
  ngOnInit(): void {
    this.user = this.userService.user;
    console.log(this.user);
  }
}
