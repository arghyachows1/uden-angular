import { Component, OnInit } from '@angular/core';
import { User } from '../../../interfaces/user.interface';
import { Router } from '@angular/router';
import { UserService } from './../../../services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  SocialAuthService,
  SocialUser,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'uden-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  user = {} as User;

  socialUser: SocialUser;
  loggedIn: boolean;
  provider: string;
  socialLogin: boolean;
  constructor(
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder,
    private authService: SocialAuthService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.authService.authState.subscribe((user) => {
      this.socialUser = user;
      this.loggedIn = user != null;
    });

    this.registerForm = this.fb.group({
      userType: ['Individual'],
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(25),
        ],
      ],
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(15),
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(25),
        ],
      ],
      email: ['', [Validators.required, Validators.email]],
      captchaVlidation: ['', [Validators.required]],
    });
  }

  changeUserType(userType) {
    this.user.userType = userType;
  }

  handleRegister(registerForm: FormGroup) {
    console.log(registerForm.value);
    this.userService.createSiteUser(registerForm.value).subscribe((data) => {
      console.log(data);
    });
    this.handleUserTypeRoute();
  }

  handleUserTypeRoute() {
    let routeurl =
      this.registerForm.get('userType').value == 'Individual'
        ? '/individualregister'
        : this.registerForm.get('userType').value == 'Corporate'
        ? '/corporateregister'
        : '/educatorregister';
    this.router.navigateByUrl(routeurl);
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data) => {
      let userData: any = data;

      console.log(data);
      this.registerForm.get('email').setValue(userData.email);
      this.registerForm.get('name').setValue(userData.name);
      this.user.name = userData.name;
      this.user.imageUrl = userData.photoUrl;
      this.provider = userData.provider;
      this.toastr.success('Signed in with Google!', 'Succesful!');
    });
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then((data) => {
      let userData: any = data;
      console.log(data);
      this.registerForm.get('email').setValue(userData.email);
      this.registerForm.get('name').setValue(userData.name);
      this.user.name = userData.name;
      this.user.imageUrl = userData.photoUrl;
      this.provider = userData.provider;
      this.toastr.success('Signed in with Facebook!', 'Succesful!');
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  refreshToken(): void {
    this.authService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved response token: ${captchaResponse}`);
  }
}
