import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from '../../../interfaces/user.interface';

@Component({
  selector: 'uden-educator-register',
  templateUrl: './educator-register.component.html',
  styleUrls: ['./educator-register.component.scss'],
})
export class EducatorRegisterComponent implements OnInit {
  constructor(private userService: UserService) {}
  user: User;
  ngOnInit(): void {
    this.user = this.userService.user;
  }
}
