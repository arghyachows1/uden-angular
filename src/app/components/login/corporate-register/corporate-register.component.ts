import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from '../../../interfaces/user.interface';

@Component({
  selector: 'uden-corporate-register',
  templateUrl: './corporate-register.component.html',
  styleUrls: ['./corporate-register.component.scss'],
})
export class CorporateRegisterComponent implements OnInit {
  constructor(private userService: UserService) {}
  user: User;
  ngOnInit(): void {
    this.user = this.userService.user;
  }
}
