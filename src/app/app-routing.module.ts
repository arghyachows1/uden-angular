import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './components/login/login-page/login-page.component';
import { RegisterComponent } from './components/login/register/register.component';
import { IndividualRegisterComponent } from './components/login/individual-register/individual-register.component';
import { CorporateRegisterComponent } from './components/login/corporate-register/corporate-register.component';
import { EducatorRegisterComponent } from './components/login/educator-register/educator-register.component';
import { IntroComponent } from './components/intro/intro.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: 'intro', pathMatch: 'full' },
  { path: 'intro', component: IntroComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'individualregister', component: IndividualRegisterComponent },
  { path: 'corporateregister', component: CorporateRegisterComponent },
  { path: 'educatorregister', component: EducatorRegisterComponent },
  { path: 'dashboard', component: DashboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
