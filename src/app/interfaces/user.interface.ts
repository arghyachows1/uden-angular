export interface User {
  userType: string;
  name: string;
  username: string;
  email: string;
  password: string;
  imageUrl: string;
  captchaVlidation: string;
}
