import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  SocialLoginModule,
  SocialAuthServiceConfig,
} from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { ContentComponent } from './components/layout/content/content.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PostRegisterComponent } from './components/login/post-register/post-register.component';
import { IndividualRegisterComponent } from './components/login/individual-register/individual-register.component';
import { CorporateRegisterComponent } from './components/login/corporate-register/corporate-register.component';
import { EducatorRegisterComponent } from './components/login/educator-register/educator-register.component';
import { RegisterComponent } from './components/login/register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginPageComponent } from './components/login/login-page/login-page.component';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { IntroComponent } from './components/intro/intro.component';
import { ToastrModule } from 'ngx-toastr';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IndividualDashboardComponent } from './components/dashboard/individual-dashboard/individual-dashboard.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    LoginPageComponent,
    RegisterComponent,
    PostRegisterComponent,
    IndividualRegisterComponent,
    CorporateRegisterComponent,
    EducatorRegisterComponent,
    IntroComponent,
    DashboardComponent,
    IndividualDashboardComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    SocialLoginModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    ToastrModule.forRoot({
      timeOut: 1500,
      positionClass: 'toast-bottom-right',
    }),
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '849880448368-78ufpppc29pamspis1guhea9k3vcoips.apps.googleusercontent.com'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('600767960827045'),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
